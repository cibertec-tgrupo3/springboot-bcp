package pe.viabcp.www.ProyectoBCP02.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "cuentasBancarias")
@Getter
@Setter

public class CuentasBancarias {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "numeroCuenta", length = 100, nullable = false)
	private String numeroCuenta;
	
	@ManyToOne
	private TipoCuenta tipoCuenta;
	
	@ManyToOne
	private Estatus estatus;
	
	@ManyToOne
	private MonedaCuenta monedaCuenta;
	
	@Column(name = "balanceTotal", precision = 10, scale = 1)
	private double balanceTotal;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "fechaApertura")
	private LocalDate fechaApertura;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "ultimaFechaUsada")
	private LocalDate ultimaFechaUsada;
	
	@ManyToOne
	private Cliente cliente;
	
	@JsonIgnore
	@OneToMany(mappedBy = "cuentasBancarias")
	private List<Transacciones> transacciones = new ArrayList<Transacciones>();


}
